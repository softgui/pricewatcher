﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Helpers;

namespace PriceWatcher
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // First Time Page Hit ?
            if (!IsPostBack)
            {
                // Build list items in dropdown control
                dlStockCodes.DataSource = StockPricingFactory.ListOfStockCodes;
                this.DataBind();

                // Select 1st item
                dlStockCodes.SelectedValue = StockPricingFactory.ListOfStockCodes[0];
            }

            // Show Stock Price
            UpdatePageStockPrice();
        }

        /// <summary>
        /// Get Price for the currently seelcted Stock Code and apply to Price textbox
        /// </summary>
        private void UpdatePageStockPrice ()
        {
            double price = StockPricingFactory.PriceByStockCode(dlStockCodes.SelectedValue);
            tbPrice.Text = $"{price:0.0000}";
        }
    }
}