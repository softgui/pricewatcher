﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Helpers
{
    public static class StockPricingFactory
    {
        private static Random randomNo = new Random();

        private static List<string> stockCodes = new List<string>(new string[]
                                {"AAE", "AAM", "AAN", "AAO", "AAQ", "AAR", "AAU", "AAX", "ABB", "ABC",
                                 "ABI", "ABJ", "AAC", "ABP", "ABQ", "ABS", "ABU", "ABY", "ACB", "ACE",
                                 "ACL", "ACR", "ADB", "ADG", "ADI", "ADL", "ADN", "ADS", "ADU", "ADX",
                                 "ADY", "ADZ", "AEC", "AED", "AEE", "AEI", "AEO", "AEP", "AET", "AEU",
                                 "AEX", "AEZ", "AFG", "AFI", "AFT", "AGC", "AGF", "AGG", "AGI", "AGK"
                                });

        #region Public Properties

        /// <summary>
        /// Gets the List of available Stock Codes to populate the Users dropdown list.
        /// Note: A dropdown list was used to aid in UI useability. No mention on requirement 
        /// for future proofing (when Stock Code list items increases)
        /// </summary>
        public static List<string> ListOfStockCodes
        {
            get { return stockCodes; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Given a Stock Code, get the current Price
        /// </summary>
        /// <param name="stockCode"></param>
        /// <returns>Stock Price</returns>
        public static double PriceByStockCode (string stockCode)
        {
            // Get Price (ie. it's just a random number 0 - 100)
            return randomNo.NextDouble() * 100;
        }

        #endregion
    }
}
